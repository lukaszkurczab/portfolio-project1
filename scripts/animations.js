const helloBox = document.querySelector(".introduce_wrapper");
const skillsPosition = document.querySelector(".graph_wrapper");
const projects = document.querySelector(".projects_wrapper");
const blog = document.querySelector(".blog_wrapper");
let projectsOffset = findPos(projects);
let blogOffset = findPos(blog);
let bottomOfSite;

let siteBottomPositionCalc = function () {
  return bottomOfSite = window.scrollY;
}

function findPos(obj) {
  var projectOffset = 0;
  if (obj.offsetParent) {
    do {
      projectOffset += obj.offsetTop;
    } while (obj = obj.offsetParent);
  }
  return projectOffset;
}

document.addEventListener("scroll", () => {
  siteBottomPositionCalc();
  switch (true) {
    case bottomOfSite > projectsOffset + 200:
      blog.classList.remove("notViewed");
      break;
    case bottomOfSite > projectsOffset - 350:
      projects.classList.remove("notViewed");
      break
    case bottomOfSite > skillsPosition.offsetTop:
      skillsPosition.classList.remove("notViewed");
      break
  }
})

window.addEventListener("load", () => {
  setTimeout(() => {
    helloBox.classList.remove("notViewed");
  }, 800)
})